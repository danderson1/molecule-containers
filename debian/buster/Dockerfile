FROM docker.io/debian:buster-slim

LABEL org.opencontainers.image.description="Debian 10 Container for Molecule"
LABEL org.opencontainers.image.source=https://gitlab.com/aussielunix/ansible/molecule-containers

ENV LC_ALL en_US.UTF-8
ENV LANG en_US.UTF-8
ENV LANGUAGE en_US.UTF-8

# Avoid apt warnings by switching to noninteractive
ENV DEBIAN_FRONTEND=noninteractive

# Configure apt and install packages
RUN apt-get update \
    && apt-get -y install --no-install-recommends systemd systemd-sysv \
    sudo procps python3-pip python3-dev python3-setuptools python3-wheel  \
    # Clean up
    && rm -rf /var/lib/apt/lists/* \
    && rm -Rf /usr/share/doc && rm -Rf /usr/share/man \
    && apt-get clean -y \
    && echo 'Australia/Sydney' > /etc/timezone

# Create `ansible` user and group with sudo permissions
RUN set -xe \
  && useradd -m -U -G sudo -s /bin/bash ansible \
  && sed -i "/^%sudo/s/ALL\$/NOPASSWD:ALL/g" /etc/sudoers

# Upgrade pip to latest version to avoid wheel / cryptography issues
RUN pip3 install --upgrade pip

# Install Ansible via pip.
RUN pip3 install ansible-core ansible-lint cryptography

# Install Ansible inventory file.
RUN mkdir -p /etc/ansible
RUN echo "[local]\nlocalhost ansible_connection=local" > /etc/ansible/hosts

# Switch back to dialog for any ad-hoc use of apt-get
ENV DEBIAN_FRONTEND=dialog

RUN rm -f /lib/systemd/system/multi-user.target.wants/* \
    /etc/systemd/system/*.wants/* \
    /lib/systemd/system/local-fs.target.wants/* \
    /lib/systemd/system/sockets.target.wants/*udev* \
    /lib/systemd/system/sockets.target.wants/*initctl* \
    /lib/systemd/system/sysinit.target.wants/systemd-tmpfiles-setup* \
    /lib/systemd/system/systemd-update-utmp*

CMD [ "/lib/systemd/systemd", "log-level=info", "unit=sysinit.target" ]
