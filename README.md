# Containers for Molecule

A collection of container images for testing [Ansible](https://www.ansible.com/) Roles with [Molecule](https://molecule.readthedocs.io/en/latest/).  
Containers will be published to the [GitLab Container Registry](https://gitlab.com/aussielunix/ansible/molecule-containers/container_registry)

The repositories [geerlingguy/docker-debian11-ansible](https://github.com/geerlingguy/docker-debian11-ansible) and [hspaans/molecule-containers](https://github.com/hspaans/molecule-containers/branches/all) were taken as starting point to create this repository.

Each container has systemd running and a non-root user, `ansible`, with passwordless sudo access.

These should all work with Systemd 248+/cgroup v2 but do require **privileged
mode.**

Example molecule.yml:

```yaml
---
dependency:
  name: galaxy
driver:
  name: docker
lint: |
  set -e
  ansible-lint
platforms:
  - name: instance
    image: "registry.gitlab.com/aussielunix/ansible/molecule-containers/${MOLECULE_DISTRO:-debian:bullseye}"
    tmpfs:
      - /run
      - /tmp
    override_command: false
    privileged: true
    pre_build_image: true
provisioner:
  name: ansible
  env:
    ANSIBLE_VERBOSITY: ${MOLECULE_ANSIBLE_VERBOSITY:-0}
verifier:
  name: ansible
```

Examples of using one of these containers with molecule:

```bash
# test with default debian:bullseye
molecule test

# add -vv to ansible and test with default debian:bullseye
MOLECULE_ANSIBLE_VERBOSITY=2 molecule test

# test with ubuntu:jammy
MOLECULE_DISTRO="ubuntu:jammy" molecule test

# add -vv to ansible and test with rockylinux:9
MOLECULE_ANSIBLE_VERBOSITY=2 MOLECULE_DISTRO="rockylinux:9" molecule test
```
